package com.robotsoft.android.cal24;

import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.widget.Toast;

public class CardPref extends PreferenceActivity implements
	Preference.OnPreferenceChangeListener {
	public static final String Deck="card_deck";
	public static final String Advanced="level_advanced";
	public static final String Duration="session_duration";
	public static final String Total="total_cards";
	public static final String Sound="sound_check";
	
	public static final String Score_30="score30";
	public static final String Score_60="score60";
	public static final String Score_120="score120";
	public static final String Score_240="score240";
	public static final String Score_480="score480";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.card);
		CheckBoxPreference cp = (CheckBoxPreference) findPreference(Sound);
		cp.setOnPreferenceChangeListener(this);
		EditTextPreference ep = (EditTextPreference) findPreference(Total);
		ep.setOnPreferenceChangeListener(this);
	}

	@Override
	public boolean onPreferenceChange(Preference preference, Object newValue) {
		if(preference instanceof CheckBoxPreference) {
			CheckBoxPreference cp = (CheckBoxPreference) preference;
			if(newValue instanceof Boolean) {
				Media.setPlaying(((Boolean)newValue).booleanValue());
			}
			return true;
		}
		if(preference instanceof EditTextPreference) {
			EditTextPreference ep = (EditTextPreference) preference;
			int n;
			try {
				n=Integer.parseInt(newValue.toString());
			}
			catch(NumberFormatException ex) {
				n=-1;
			}
			if(n>52||n<8) {
				Toast.makeText(this, R.string.invalid_card_number, Toast.LENGTH_LONG).show();
				return false;
			}
		}
		
		return true;
	}
	

}
