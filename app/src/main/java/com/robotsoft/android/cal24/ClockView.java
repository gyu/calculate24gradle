package com.robotsoft.android.cal24;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.RectF;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

public class ClockView extends ButtonExt {
	private int tip2;
	private int timeSpan;
	private CalculateActivity host;
	private Handler handler;
	private long startTime=-2;
	private long paused=-1;
	private int score;
	private int timeUsed;

	public ClockView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public ClockView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public ClockView(Context context) {
		super(context);
	}
	
	@Override
	protected void init(Context context) {
		super.init(context);
		host=(CalculateActivity)context;
		float paddingScale = context.getResources().getDisplayMetrics().density;
		tip2 = (int) (2* paddingScale + 0.5);
		handler=new Handler();
	}
	
	@Override
	protected void doDraw(Canvas canvas) {
		if(timeSpan<=0||startTime<0) return;
		int w=this.getWidth();
		int h=this.getHeight();
		Paint p=new Paint();
		p.setColor(0xff009f00);
		p.setAntiAlias(true);
		p.setStyle(Style.FILL);
		int r=Math.min(w,h)*3/8;
		long t=System.currentTimeMillis();
		if(t+50>=startTime+timeSpan) {
			canvas.drawCircle(w/2, h/2, r-1, p);
		}
		else {
			RectF oval=new RectF(w/2-r+1,h/2-r+1,w/2+r-1,h/2+r-1);
			canvas.drawArc(oval, 270, (t-startTime)*360/timeSpan, true, p);
		}
		p=new Paint();
		Paint p1=getPaint();
		p.setColor(p1.getColor());
		p.setAntiAlias( true );
		p.setStyle(Style.STROKE);
		p.setStrokeWidth(tip2);
		canvas.drawCircle(w/2, h/2, r, p);
	}

	public void pause() {
		if(paused==-1) {
			paused=System.currentTimeMillis();
		}
	}
	
	public void stop() {
		pause();
		timeUsed=(int)(System.currentTimeMillis()-startTime);
		startTime=-2;
	}
	
	public int getScore(boolean win) {
		int f=1;
		if(win) {
			if(timeUsed*2>timeSpan) {
				f=4;
			}
			else if(timeUsed*4>timeSpan) {
				f=2;
			}
			
		}
		return score/f;
	}

	public int getTimeSpan() {
		return timeSpan;
	}

	public void start() {
		paused=-1;
		startTime = System.currentTimeMillis();
		handler.postDelayed(new Timer(), 300);
	}

	public void resume() {
		if(paused!=-1) {
			long t=System.currentTimeMillis();
			startTime+=t-paused;
			paused=-1;
			handler.postDelayed(new Timer(),Math.min(timeSpan/120, (int)(startTime+timeSpan-t)+100));
		}
	}

	private class Timer implements Runnable {
		@Override
		public void run() {
			ClockView.this.invalidate();
			if(paused!=-1) {
			}
			else {
				long t=System.currentTimeMillis();
				if(startTime<=0) {
					startTime=t;
				}
				if(t>startTime+timeSpan) {
					Log.e("Cal24",t+"   "+startTime+"  "+timeSpan);
					
					
					host.expire();
				}
				else {
					handler.postDelayed(new Timer(),Math.min(timeSpan/120, (int)(startTime+timeSpan-t)+100));		
				}
			}
		}
	}

	public void reset() {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(host);
		int t = Util.getInt(prefs, CardPref.Duration, 240);
		if(t<=30) {
			score=Util.getInt(prefs, CardPref.Score_30, 320);
		}
		else if(t<=60) {
			score=Util.getInt(prefs, CardPref.Score_60, 160);
		}
		else if(t<=120) {
			score=Util.getInt(prefs, CardPref.Score_120, 80);
		}
		else if(t<=240) {
			score=Util.getInt(prefs, CardPref.Score_240, 40);
		}
		else {
			score=Util.getInt(prefs, CardPref.Score_480, 20);
		}
		timeSpan=t*1000;
	}

	public void showTimeLeft() {
		
		if(startTime>0) {
			int t=(int)((startTime+timeSpan-System.currentTimeMillis())/1000);
			Toast.makeText(getContext(), getContext().getString(R.string.time_left)+": "+t+" sec", Toast.LENGTH_LONG).show();
		}
	}
	
}
