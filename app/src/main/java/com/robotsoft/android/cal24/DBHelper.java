package com.robotsoft.android.cal24;

import static android.provider.BaseColumns._ID;
import static com.robotsoft.android.cal24.DBConstants.*;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {
	private static final String DATABASE_NAME = "cal24.db";
	private static final int DATABASE_VERSION = 1;
	private String statsSql() {
		return "CREATE TABLE " + StatsTable + " (" 
				+ TimeSpan + " INTEGER," + CardNumber + " INTEGER,"+ Score + " INTEGER," + Advanced
				+ " INTEGER);";
	}


	public DBHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(statsSql());
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS " + StatsTable);
		onCreate(db);
	}

}
