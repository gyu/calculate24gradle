/*
 * Copyright (C) 2008 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.robotsoft.android.cal24;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.KeyEvent;
import android.widget.Button;
import android.widget.EditText;

public class EventListener implements View.OnKeyListener, 
                               View.OnClickListener, 
                               View.OnLongClickListener {
	private boolean advanced;
	private CalculateActivity host;
	private EditText input;
	private ClockView clock;
    
    //@Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
        case R.id.submit:
        	host.doSubmit();
            break;
        case R.id.giveup:
        	host.doGiveup();
            break;
        case R.id.back:
        	int l=input.length();
        	if(l>0) {
        		int cursor = input.getSelectionStart();
        		if(cursor>0&&cursor<=l) {
        			String s=input.getText().toString();
        			input.setText(s.substring(0,cursor-1)+s.substring(cursor));
        			input.setSelection(cursor-1);
        		}
        	}
            break;
        case R.id.clear:
        	onClear();
            break;
        case R.id.clock:
        	Log.d("Cal24","Ask show time");
        	clock.showTimeLeft();
            break;   
            
        default:
            if (view instanceof Button) {
                String text = ((Button) view).getText().toString();
                char ch=(text.equals("10"))?'0':text.length()!=1?0:text.charAt(0);
                insert(ch);
            }
        }
    }
    
    private void insert(char c) {
    	String s;
    	switch(c) {
    	case 0:return;
    	case '0':s="10";break;
    	case 'A':s="1";break;
    	case 'K':s=advanced?"13":"1";break;
    	case 'Q':s=advanced?"12":"1";break;
    	case 'J':s=advanced?"11":"1";break;
    	default:s=String.valueOf(c);break;
    	}
        int cursor = input.getSelectionStart();
        input.getText().insert(cursor, s);
    }
    

    
    void onClear() {
    	input.setText("");
    }

    //@Override
    public boolean onLongClick(View view) {
        int id = view.getId();
        if (id == R.id.clear) {
            onClear();
            return true;
        }
        return false;
    }

    //@Override
    public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
        int action = keyEvent.getAction();
        
        if (keyCode == KeyEvent.KEYCODE_DPAD_LEFT ||
            keyCode == KeyEvent.KEYCODE_DPAD_RIGHT) {
        	boolean toLeft=(keyCode == KeyEvent.KEYCODE_DPAD_LEFT);
            int cursorPos = input.getSelectionStart();
            return toLeft ? cursorPos == 0 : cursorPos >= input.length();
        }

        //Work-around for spurious key event from IME, bug #1639445
        if (action == KeyEvent.ACTION_MULTIPLE && keyCode == KeyEvent.KEYCODE_UNKNOWN) {
            return true; // eat it
        }

        if (keyCode != KeyEvent.KEYCODE_DPAD_CENTER &&
            keyCode != KeyEvent.KEYCODE_DPAD_UP &&
            keyCode != KeyEvent.KEYCODE_DPAD_DOWN &&
            keyCode != KeyEvent.KEYCODE_ENTER) {
            return false;
        }

        if (action == KeyEvent.ACTION_UP) {
            switch (keyCode) {                
            case KeyEvent.KEYCODE_ENTER:
            case KeyEvent.KEYCODE_DPAD_CENTER:
                host.doSubmit();
                break;
            }
        }
        return true;
    }

	public void setHost(CalculateActivity calculateActivity, EditText input, ClockView clock) {
		this.host=calculateActivity;
		this.input=input;
		this.clock=clock;
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(calculateActivity);
		advanced = prefs.getBoolean(CardPref.Advanced, true);
	}

	public boolean isAdvanced() {
		return advanced;
	}

	public void reset(Context ctx) {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ctx);
		advanced = prefs.getBoolean(CardPref.Advanced, true);	
	}
}
