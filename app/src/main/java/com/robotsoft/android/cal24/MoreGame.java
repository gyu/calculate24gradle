package com.robotsoft.android.cal24;

import android.app.Activity;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TextView;

public class MoreGame extends Activity {
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.more_game);
    }
    
    private void goMarket(String marketId) {
    	try {
    		Intent goToMarket =  new Intent(Intent.ACTION_VIEW,Uri.parse("https://market.android.com/details?id="+marketId));
    		startActivity(goToMarket);
    	}catch(Throwable e) {
    		Log.e("RobotSoft","Error goto market.",e);
    	}
    	finish();
    }

    @SuppressWarnings({"UnusedDeclaration"})
    public void jigsawSelected(View v) {
    	goMarket("com.robotsoft.android.jigsaw");
    }

    @SuppressWarnings({"UnusedDeclaration"})
    public void puzzleSelected(View v) {
    	goMarket("com.robotsoft.android.sliding");
    }
	
    @SuppressWarnings({"UnusedDeclaration"})
    public void rubikSelected(View v) {
    	goMarket("com.robotsoft.android.magic");
    }
    
    @SuppressWarnings({"UnusedDeclaration"})
    public void tangramSelected(View v) {
    	goMarket("com.robotsoft.android.tangram");
    }

    @SuppressWarnings({"UnusedDeclaration"})
    public void huarongdaoSelected(View v) {
    	goMarket("com.robotsoft.android.huarongdao");;
    }

    @SuppressWarnings({"UnusedDeclaration"})
    public void cal24Selected(View v) {
    	goMarket("com.robotsoft.android.cal24");;
    }

    
    @SuppressWarnings({"UnusedDeclaration"})
    public void missnoSelected(View v) {
    	goMarket("com.robotsoft.android.gps");;
    }
    
    @SuppressWarnings({"UnusedDeclaration"})
    public void sudokuSelected(View v) {
    	goMarket("sudoku");
    }

    @SuppressWarnings({"UnusedDeclaration"})
    public void tetrisSelected(View v) {
    	goMarket("cal24");
    }
	@Override
	public void onAttachedToWindow() {
		super.onAttachedToWindow();
		Window window=getWindow();
		window.setFormat(PixelFormat.RGBA_8888);
	}

}
