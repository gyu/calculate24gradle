package com.robotsoft.android.cal24;
import android.content.Context;
import android.media.MediaPlayer;
import android.util.Log;

public class Media {
	private static MediaPlayer player;
	private static boolean playing=false; 
	
	public static void step(Context context) {
		if(playing)
		play(context, R.raw.bar, false);
	}

	public static void congratulate(Context context) {
		if(playing)
			play(context, R.raw.fanfare, false);
	}
	
	public static void feelSorry(Context context) {
		if(playing)
			play(context, R.raw.fanfare, false);
	}

	
	private static void play(Context context, int resId, boolean loop) {
		try {
			Log.d("Cal24", "Play Sound");
			MediaPlayer	player = MediaPlayer.create(context, resId);
			player.setVolume(1f, 1f);
			player.setLooping(loop);
			player.start();
		}catch(Exception ex) {
			Log.e("Cal24", "Cannot create media player", ex);
		}
	}

	public static boolean isPlaying() {
		return player!=null;
	}

	public static boolean canPlay() {
		return true;
	}
	
	public static void start(final Context context, final int resId) {
		if(!playing) {
			Runnable runnable=new Runnable(){
				@Override
				public void run() {
					try {
						Log.d("Cal24", "Play Sound Clip");
						player = MediaPlayer.create(context, resId);
						player.setVolume(0.5f, 0.5f);
						player.setLooping(true);
						player.start();

					}catch(Exception ex) {
						player=null;
					}
				}};
				new Thread(runnable).start();
		}		
	}
	
	public static void stop() {
		if(player!=null) {
			player.stop();
			player=null;
		}
	}

	public static void setPlaying(boolean booleanValue) {
		Log.d("Cal24","Playing sound "+booleanValue);
		
		
		playing=booleanValue;
		stop();
	}

}
