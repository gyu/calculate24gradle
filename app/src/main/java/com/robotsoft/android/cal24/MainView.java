package com.robotsoft.android.cal24;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Point;
import android.graphics.RectF;
import android.preference.PreferenceManager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

public class MainView extends View {
	protected int width = -1;
	protected int height = -1;
	private List<Card> computer;
	private List<Card> player;
	private Item[] current = new Item[4];
	public static int CardWidth = 80;
	public static int CardHeight = 123;
	private Bitmap deck;
	private int score;
	private int totalCards;
	private int frameCount=-1;
	private int startFrameCount=-1;
	private int totalFrames=60;
	private int stage1=5;
	private int stage2=21;
	private int stage3=51;
	private int startStage1=6;
	private int startTotalFrames=14;
	private Item[] last;

	public MainView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public MainView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public MainView(Context context) {
		super(context);
	}
	
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		int w = getMeasuredWidth();
		int h = getMeasuredHeight();
		Log.d("Cal", "onMeasure " + w + " " + h + ",  " + width + " " + height);
		if (width == -1 || width != w || height != h) {
			initView(w, h);
		}
	}
	
	private void drawBitmap(Canvas canvas, Bitmap bmp, int x, int y) {
		int w=bmp.getWidth();
		x+=(CardWidth-w)/2;
		canvas.drawBitmap(bmp,x, y, null);
		Paint p=new Paint();
		p.setColor(0xff002010);
		p.setAntiAlias(true);
		p.setStyle(Style.STROKE);
		p.setStrokeWidth(1.2f);
		canvas.drawLine(x-1, y+3, x-1, y+CardHeight-3, p);
		canvas.drawLine(x+w, y+3, x+w, y+CardHeight-3, p);
		RectF rf=new RectF(x-1,y,x+5,y+6);
		canvas.drawArc(rf, 180, 90, false, p);
		rf=new RectF(x-1,y+CardHeight-6,x+5,y+CardHeight);
		canvas.drawArc(rf, 90, 90, false, p);
		rf=new RectF(x+w-6,y,x+w,y+6);
		canvas.drawArc(rf, 270, 90, false, p);
		rf=new RectF(x+w-6,y+CardHeight-6,x+w,y+CardHeight);
		canvas.drawArc(rf, 0, 90, false, p);
	}
	
	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		if (width == -1 || width != w || height != h) {
			initView(w, h);
		}
		super.onSizeChanged(w, h, oldw, oldh);
	}
	
	public boolean startFrame() {
		boolean b;
		if (startFrameCount == -1) {
			if(current==null||current[0]==null||computer==null) {
				startFrameCount = startStage1+1;
				createNew();
			}	
			else	
				startFrameCount = 1;
			b = true;
		} else {
			startFrameCount++;
			b=startFrameCount<startTotalFrames;
			if(!b) {
				startFrameCount = -1;
				
			}else if(startFrameCount==startStage1) {
				createNew();
			}
		}
		invalidate();
		return b;
	}

	
	public boolean nextFrame() {
		boolean b;
		if (frameCount == -1) {
			if(last==null) {
				frameCount = stage2+1;
			}
			else {
				frameCount = 1;
			}
			b = true;
			
		} else {
			frameCount++;
			if (frameCount == totalFrames) {
				frameCount = -1;
				b = false;
			} else {
				if(frameCount == stage2&&last!=null) {
					if(score!=0) {
						List<Card> c=score>0?player:computer;
						for(int i=0; i<4; i++)c.add(last[i].card);
					}
					else {
						computer.add(last[0].card);
						computer.add(last[1].card);
						player.add(last[2].card);
						player.add(last[3].card);
					}
					last=null;
				}
				else if(frameCount == stage3) {
					for(int i=0; i<4; i++) {
						current[i].getCard();
					}
					for(int i=0; i<4; i++) {
						current[i].removeFromList();
					}	
				}
				b = true;
			}	
		}
		invalidate();
		return b;
	}
	
	private void drawStartSet(Canvas canvas) {
		try {
			if(startFrameCount<startStage1) {
				float rate=1f*startFrameCount/(startStage1*1.1f);
				drawDeckDisappear(canvas,computer.size(), true, rate );
				drawDeckDisappear(canvas,player.size(), false, rate);
				int tx=width*3/2-CardWidth/2;
				for (int i = 0; i < 4; i++) {
					Item m=current[i];
					Point p=m.position(i, width, height);
					drawBitmap(canvas, m.image,(int)Math.round(p.x+(tx-p.x)*rate), p.y);
				}
			}
			else if(startFrameCount>startStage1) {
				float rate=1f*((startTotalFrames-startStage1)*0.25f+startFrameCount-startStage1)/((startTotalFrames-startStage1)*1.25f);
				drawDeckAppear(canvas,computer.size(), true, rate );
				drawDeckAppear(canvas,player.size(), false, rate);
			}
			
		}catch(Throwable e) {
			Log.e("Cal24","Start Draw Animation",e);
		}
	}
	
	private void drawNewSet(Canvas canvas) {
		try {
			if(frameCount<=stage1) {
				drawDeck(canvas,computer.size(), true );
				drawDeck(canvas,player.size(), false);
				float f=1f*frameCount/stage1;
				for (int i = 0; i < 4; i++) {
					Item m=last[i];
					Point p=m.position(i, width, height);
					Bitmap bmp=Util.rotate(m.image, deck,f);
					if(bmp!=null) {
						drawBitmap(canvas, bmp, p.x, p.y);
					}
				}
			}
			else if(frameCount<stage2){
				drawDeck(canvas,computer.size(), true);
				drawDeck(canvas, player.size(), false);
				float f=1f*(frameCount-stage1)/(stage2-stage1);
				for (int i = 0; i < 4; i++) {
					Item im = current[i];
					Point p0 = im.position(i, width, height);
					boolean top;
					int index;
					int s;
					if(score==0) {
						top=(i<2);
						List<Card> lst=(top)?computer:player;
						s=lst.size()+2;
						index=lst.size()+((i>2)?i-2:i);
					}	
					else if(score<0) {
						top=true;
						s=computer.size()+4;
						index=computer.size()+i;
					}
					else  {
						top=false;
						s=player.size()+4;
						index=player.size()+i;
					}
					Point p1 = stackPosition(s, index, top);
					int x =Math.round( p0.x + (p1.x - p0.x) * f);
					int y =Math.round(p0.y + (p1.y - p0.y) * f);
					drawBitmap(canvas,deck, x, y);
				}
			}
			else if(frameCount==stage2){
				drawDeck(canvas, computer.size(),  true);
				drawDeck(canvas, player.size(), false);
			}
			else if(frameCount<stage3) {
				drawDeck(canvas, computer.size(), current[0].index,
						current[1].index, true);
				drawDeck(canvas, player.size(), current[2].index,
						current[3].index, false);
				float f=1f*(frameCount-stage2)/(stage3-stage2);
				for (int i = 0; i < 4; i++) {
					Item im = current[i];
					Point p1 = im.position(i, width, height);
					Point p0 = stackPosition(im.list.size(), im.index, i < 2);
					int x =Math.round( p0.x + (p1.x - p0.x) * f);
					int y =Math.round(p0.y + (p1.y - p0.y) * f);
					drawBitmap(canvas,deck, x, y);
				}
			}
			else if(frameCount==stage3) {
				drawDeck(canvas, computer.size(), true);
				drawDeck(canvas, player.size(), false);
				for (int i = 0; i < 4; i++) {
					Item im = current[i];
					Point p = im.position(i, width, height);
					drawBitmap(canvas,deck, p.x, p.y);
				}
			}
			else {
				drawDeck(canvas,computer.size(), true );
				drawDeck(canvas,player.size(), false);
				float f=1f*(frameCount-stage3)/(totalFrames-stage3);
				for (int i = 0; i < 4; i++) {
					Item m=current[i];
					Point p=m.position(i, width, height);
					Bitmap bmp=Util.rotate(deck,m.image, f);
					drawBitmap(canvas, bmp, p.x, p.y);
			}
			}
		}catch(Throwable e) {
			Log.e("Cal24","Error Draw Animation",e);
		}
		
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		if(startFrameCount!=-1) {
			drawStartSet(canvas);
		}
		else if(frameCount!=-1) {
			drawNewSet(canvas);
		}
		else {
			if(deck!=null) {
				drawDeck(canvas,computer.size(), true );
				drawDeck(canvas,player.size(), false);
			}
			if (current[0] != null&&current[0].card!=null) {
				for (int i = 0; i < 4; i++) {
					Item m=current[i];
					Point p=m.position(i, width, height);
					drawBitmap(canvas,m.image, p.x, p.y);
				}
			}
		}
	}

	
	private void initView(int w, int h) {
		width = w;
		height = h;
	}
	
	public int[] getCards() {
		int[] res=new int[4];
		for(int i=0; i<4; i++) {
			res[i]=current[i].getCard().getValue();
		}
		Arrays.sort(res);
		return res;
	}
	
	private void createNew() {
		fetchPref();
		Card[] cs = new Card[52];
		Card[] all = Card.values();
		for (int i = 0; i < 52; i++) {
			cs[i] = all[i];
		}
		Random rnd = new Random();
		for (int i = 0, n = 1000 + rnd.nextInt(1000); i < n; i++) {
			int j = rnd.nextInt(52);
			int k = rnd.nextInt(52);
			if (j != k) {
				Card c = cs[j];
				cs[j] = cs[k];
				cs[k] = c;
			}
		}
		if(computer==null) {
			computer = new ArrayList<Card>();
			player = new ArrayList<Card>();
		}
		else {
			computer.clear();
			player.clear();
		}
		for (int i = 0; i < totalCards/2; i++) {
			computer.add(cs[i]);
			player.add(cs[i + 26]);
		}
		current[0]=null;
		current[1]=null;
	}
	
	private void fetchPref() {
		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(getContext());
		deck = BitmapFactory.decodeResource(getResources(), DeckPreference.getResourceId(prefs));
		int w=deck.getWidth();
		int h=deck.getHeight();
		if(w!=CardWidth||h!=CardHeight) {
			CardWidth=w;
			CardHeight=h;
		}
		totalCards=(Util.getInt(prefs, CardPref.Total, 52)/2)*2;
		if(totalCards>52)totalCards=52;
		else if(totalCards<8)totalCards=8;
	}

	private Point couple(Random rnd, int n) {
		int j;
		int k;
		if (n > 2) {
			 j=rnd.nextInt(n);
			 k=rnd.nextInt(n-1);
			 if(k>=j) {
				 k++;
			 }
			 else {
				int l=k;
				k=j;
				j=l;
			}
			
		} else {
			j=0;
			k=1;
		}
		return new Point(j,k);
	}
	
	public int getStatus() {
		if(computer.size()==0) {
			if(score>0) return 1;
		}
		if(player.size()==0) {
			Log.d("Cal24","///// Size is 0 ????");
			if(score<0) return -1;
		}
		return 0;
	}

	public int[] pickNext() {
		last=null;
		if(current[0]!=null) {
			last=new Item[4];
			for(int i=0; i<4; i++) {
				last[i]=current[i];
			}
		}
		int[] vals=new int[4];
		Random rnd = new Random();
		Resources res = getContext().getResources();
		Point p;
		if(computer.size()>0) {
			p=couple(rnd,computer.size());
			vals[0]=computer.get(p.x).getValue();
			vals[1]=computer.get(p.y).getValue();
			current[0] = new Item(res, computer, p.x);
			current[1] = new Item(res, computer, p.y);
		}
		else  {
			int i1;
			int i2;
			if(score==0) {
				i1=0;
				i2=1;
			}
			else {
				i1=rnd.nextInt(4);
				i2=rnd.nextInt(3);
				if(i2>=i1) {
					i2++;
				}
				else {
					int j=i1;
					i1=i2;
					i2=j;
				}
			}
			vals[0] = last[i1].card.getValue();
			vals[1] = last[i2].card.getValue();
			current[0] = new Item(computer, i1,last[i1].image);
			current[1] = new Item(computer, i2,last[i2].image);
		}
		if(player.size()>0) {
			p=couple(rnd,player.size());
			vals[2]=player.get(p.x).getValue();
			vals[3]=player.get(p.y).getValue();
			current[2] = new Item(res, player, p.x);
			current[3] = new Item(res, player, p.y);
		}
		else  {
			int i1;
			int i2;
			int j1;
			int j2;
			if(score==0) {
				i1=2;
				i2=3;
				j1=0;
				j2=1;
			}
			else {
				i1=rnd.nextInt(4);
				i2=rnd.nextInt(3);
				if(i2>=i1) {
					i2++;
				}
				else {
					int j=i1;
					i1=i2;
					i2=j;
				}
				j1=i1;
				j2=i2;
			}
			vals[2] = last[i1].card.getValue();
			vals[3] = last[i2].card.getValue();
			current[2] = new Item(player, j1,last[i1].image);
			current[3] = new Item(player, j2,last[i2].image);
		}
		return vals;
	}

	private Point stackPosition(int count, int current, boolean top) {
		int span=height/20;
		int y=top?span:height-span-CardHeight;
		int x;
		int dx;
		if(count==1) {
			x=width/2-CardWidth/2;
			dx=0;
		}
		else {
			span = width/30;
			int total=width-2*span;
			int margin=CardWidth/4;
			if((count-1)*margin+count*CardWidth<total) {
				total=(count-1)*margin+count*CardWidth;
				x=width/2-total/2;
				dx=margin+CardWidth;
			}
			else {
				x=span;
				dx=(total-CardWidth)/(count-1);
			}
		}
		return new Point(x+dx*current, y);
	}
	
	private void drawDeck(Canvas canvas,int count, int a, int b, boolean top ) {
		for (int i = 0; i < count; i++) {
			if(i!=a&&i!=b) {
			Point p=stackPosition(count, i, top);
			drawBitmap(canvas, deck, p.x, p.y);
			}
		}
	}
	
	private void drawDeck(Canvas canvas,int count, boolean top ) {
		drawDeck(canvas, count, -1, -1,  top );
	}

	private void drawDeckDisappear(Canvas canvas,int count, boolean top,float rate ) {
		int tx=width*3/2-CardWidth/2;
		int ty=height/2-CardHeight/2;
		for (int i = 0; i < count; i++) {
			Point p=stackPosition(count, i, top);
			drawBitmap(canvas,deck, (int)(p.x+(tx-p.x)*rate),(int)( p.y+(ty-p.y)*rate));
		}
	}
	
	private void drawDeckAppear(Canvas canvas,int count, boolean top,float rate ) {
		int tx=-width/2-CardWidth/2;
		int ty=height/2-CardHeight/2;
		for (int i = 0; i < count; i++) {
			Point p=stackPosition(count, i, top);
			drawBitmap(canvas,deck,(int)( tx+(p.x-tx)*rate),(int)( ty+(p.y-ty)*rate));
		}
	}

	public void changeCard(int score) {
		this.score=score;
	}
	
	public int getTotalCards() {
		return totalCards;
	}


	private static class Item {
		Card card;
		Bitmap image;
		List<Card> list;
		int index;

		Item(Resources res, List<Card> list, int index) {
			this.list=list;
			this.index=index;
			image = BitmapFactory.decodeResource(res, list.get(index).getResId());
		}
		
		Item(List<Card> list, int index, Bitmap bmp) {
			this.list=list;
			this.index=index;
			image = bmp;
		}
		
		Point position(int pos, int width, int height) {
			int y = height / 2 - CardHeight / 2;
			int start=50;
			if(CardWidth*5+50<width) {
				if((width-CardWidth*5)/2>width * 3 / 16) {
					start = width * 3 / 16;
				}
				else {
					start=(width-5*CardWidth)/2;
				}
			}
			int dx = ((width-2*start) - CardWidth) / 3;
			return new Point(start+pos*dx,y);
		}
		
		Card getCard() {
			if(card==null) {
				card=list.get(index);	
			}
			return card;
		}
		
		void removeFromList() {
			if(list!=null)  {
				list.remove(card);
				list=null;	
			}
			
		}
	}


}
