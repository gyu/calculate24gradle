package com.robotsoft.android.cal24;

import static com.robotsoft.android.cal24.DBConstants.*;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;

public class Util {
	
	public static StatsItem[] getStats(Activity ctx) {
		List<StatsItem> list = new ArrayList<StatsItem>();
		Cursor cursor = null;
		try {
			cursor=((Activity)ctx).managedQuery(DataProvider.getStatsTableUri(), new String[]{TimeSpan,CardNumber,Score, Advanced}, null,  null, 
					Score+" desc,"+Advanced+" desc,"+TimeSpan+" desc,"+CardNumber);
			StatsItem current=null;
			while (cursor.moveToNext()) {
				int timeSpan=cursor.getInt(0);
				int cardNumber=cursor.getInt(1);
				int score=cursor.getInt(2);
				boolean advanced=(cursor.getInt(3)==1);
				if(current==null||timeSpan!=current.getTimeSpane()||cardNumber!=current.getCardNumber()||score!=current.getScore()||advanced!=current.isAdvanced()) {
					current=new StatsItem(timeSpan,cardNumber,score,advanced);
					list.add(current);
				}
				else {
					current.addCount();
				}
			}
		} catch (Exception ex) {
			Log.e("Cal24", "Get stats error", ex);
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}
		return list.toArray(new StatsItem[list.size()]);
	}
	
	public static void addStats(Activity ctx, StatsItem s) {
		ContentValues v = new ContentValues();
		v.put(TimeSpan, s.getTimeSpane());
		v.put(CardNumber, s.getCardNumber());
		v.put(Score, s.getScore());
		v.put(Advanced, s.isAdvanced()?1:0);
		ctx.getContentResolver().insert(DataProvider.getStatsTableUri(), v);
	}
	
	public static void clearStats(Activity ctx) {
		ctx.getContentResolver().delete(DataProvider.getStatsTableUri(), null, null);
	}
	
	public static Bitmap rotate(Bitmap a, Bitmap b,float oitar) {
		if(oitar<0.02) return a;
		if(oitar>0.98) return b;
		if(Math.abs(oitar-0.5)<1e-6) return null;
		int w=a.getWidth();
		int nw;
		int h=a.getHeight();
		int[] u=new int[w*h];
		float q;
		if(oitar<0.5) {
			nw=(int)((1-oitar*2)*w);
			a.getPixels(u, 0, w, 0, 0, w, h);
			q=1/(1-oitar*2);
		}
		else {
			nw=(int)((oitar*2-1)*w);
			b.getPixels(u, 0, w, 0, 0, w, h);
			q=1/(oitar*2-1);
		}
		if(nw<1) return null;
		int[] z=new int[nw*h];
		for(int y=0, i=0; y<h; y++) {
			float s=0;
			for(int x=0,j=y*w; x<nw; x++,i++,s+=q) {
				int ix=(int)s;
				if(ix>=w)ix=w-1;
				z[i]=u[j+ix];
			}
		}
		Bitmap bmp=Bitmap.createBitmap(nw,h,Bitmap.Config.ARGB_8888);
		bmp.setPixels(z, 0, nw, 0, 0, nw, h);
		return bmp;
	}

	
	public static Bitmap transit(Bitmap a, Bitmap b,float oitar) {
		if(oitar<0.02) return a;
		if(oitar>0.98) return b;
		int w=a.getWidth();
		float ratio=(1-oitar);
		int s=(int)(w*ratio);
		if(w-s<10) return a;
		if(s<10) return b;
		int h=a.getHeight();
		int n=w*h;
		int[] u=new int[n];
		a.getPixels(u, 0, w, 0, 0, w, h);
		int[] v=new int[n];
		b.getPixels(v, 0, w, 0, 0, w, h);
		int [] z=new int[n];
		float p=1/oitar;
		float q=1/ratio;
		for(int y=0,i=0; y<h; y++) {
			int j=y*w;
			float xx=0;
			for(int x=0; x<s; x++,i++,xx+=q) {
				int ix=(int)xx;
				if(ix>=w)ix=w-1;
				z[i]=u[j+ix];
			}
			xx=0;
			for(int x=s; x<w; x++,i++,xx+=p) {
				int ix=(int)xx;
				if(ix>=w)ix=w-1;
				z[i]=v[j+ix];
			}
		}
		Bitmap bmp=Bitmap.createBitmap(w,h,Bitmap.Config.ARGB_8888);
		bmp.setPixels(z, 0, w, 0, 0, w, h);
		Log.i("Cal24","Transmit bitmap created.");
		return bmp;
	}
	
	public static int getInt(SharedPreferences prefs, String key, int defVal) {
		try {
			return prefs.getInt(key, defVal);
			
		}catch(Exception ex) {
			return Integer.parseInt(prefs.getString(key, String.valueOf(defVal)));
			
		}
	}

}
