package com.robotsoft.android.cal24;

import android.os.Bundle;
import android.preference.PreferenceActivity;

public class ScorePref extends PreferenceActivity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.score);
	}

}
