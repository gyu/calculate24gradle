package com.robotsoft.android.cal24;

import android.content.Context;

public class SyntaxException extends Exception {
	private int stringId=-1;
	
	public SyntaxException(String message) {
		super(message);
	}
	
	public SyntaxException(int id) {
		super();
		stringId=id;
	}
	
	public String getMessage(Context ctx) {
		if(stringId==-1) return getMessage();
		return ctx.getString(stringId);
	}

}
