package com.robotsoft.android.cal24;
import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TabHost;

public class PrefManager extends TabActivity {

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		try {
			initTab(CardPref.class, "general", R.string.general,
					R.drawable.tab_general);
			initTab(ScorePref.class, "score", R.string.score,
					R.drawable.tab_score);
			getTabHost().setCurrentTab(0);
		} catch (Throwable e) {
			Log.e("Cat24", "Load Preference Error", e);
		}
	}
	
	private void initTab(Class clz, String key, int txtId, int iconId) {
		Intent intent = new Intent(this, clz);
		TabHost.TabSpec spec = getTabHost()
				.newTabSpec(key)
				.setIndicator(getString(txtId),
						getResources().getDrawable(iconId)).setContent(intent);
		getTabHost().addTab(spec);
	}

}
