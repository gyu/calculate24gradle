package com.robotsoft.android.cal24;

import java.util.Locale;
import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class HelpActivitiy extends Activity{
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.help);
		WebView view = (WebView) findViewById(R.id.help_page);
		String uri="help.html";
		view.getSettings().setJavaScriptEnabled(true);
		view.loadUrl("file:///android_asset/"+uri);
		view.setWebViewClient(new WebViewClient() {
			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				view.loadUrl(url);
				return true;
			}
		});
		view.setBackgroundColor(getResources().getColor(R.color.style_background));
	}


}
