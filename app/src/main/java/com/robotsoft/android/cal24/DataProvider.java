package com.robotsoft.android.cal24;

import static com.robotsoft.android.cal24.DBConstants.DataAutority;
import static com.robotsoft.android.cal24.DBConstants.StatsTable;
import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;


public class DataProvider extends ContentProvider {
	private DBHelper helper;
	private static final int Stats = 1;
    private static final UriMatcher Matcher = new UriMatcher(UriMatcher.NO_MATCH);
    private static Uri StatsTableUri;
    
    static {
		Matcher.addURI(DataAutority, StatsTable, Stats);
		StatsTableUri=Uri.parse("content://"+ DataAutority + "/" + StatsTable);
    }
    
    public static Uri getStatsTableUri() {
		return StatsTableUri;
	}


	private String matchTable(Uri uri) {
    	if(Matcher.match(uri)==Stats) {
			return StatsTable;
		}
		return null;
    }
    
    
	@Override
	public int delete(Uri uri, String selection,
	         String[] selectionArgs) {
		String table=matchTable(uri);
		if(table!=null) {
			 SQLiteDatabase db = helper.getWritableDatabase();
		     int count=db.delete(table, selection, selectionArgs);
		     getContext().getContentResolver().notifyChange(uri, null);
		     return count;	
		}
		return 0;
	}

	@Override
	public String getType(Uri uri) {
		String table=matchTable(uri);
		if(table!=null) {
			return "vnd.android.cursor.dir/com.robotsoft.android.data."+table;
		}
		return null;
	}
	@Override
	public Uri insert(Uri uri, ContentValues values) {
		String table=matchTable(uri);
		if(table!=null) {
	     SQLiteDatabase db = helper.getWritableDatabase();
	     long id = db.insertOrThrow(table, null, values);
	     if(id>0) { 
	    	 String s="content://"+ DataAutority + "/" + table;	 
	    	 Uri newUri =ContentUris.withAppendedId(Uri.parse(s), id);
	    	 getContext().getContentResolver().notifyChange(newUri, null);
	    	 return newUri;
	     }
		}
	     return null;
	}
	@Override
	public boolean onCreate() {
		helper=new DBHelper(getContext());
		return true;
	}
	
	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {
		String table=matchTable(uri);
		if(table!=null) {
		   SQLiteDatabase db = helper.getReadableDatabase();
	      Cursor cursor = db.query(table, projection, selection,
	            selectionArgs, null, null, sortOrder);
	      cursor.setNotificationUri(getContext().getContentResolver(),
	            uri);
	      return cursor;
		}
		return null;
	}
	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {
		String table=matchTable(uri);
		if(table!=null) {
	     SQLiteDatabase db = helper.getWritableDatabase();
	     int count = db.update(table, values, selection,
	               selectionArgs);
	     getContext().getContentResolver().notifyChange(uri, null);
		return count;
		}
		return 0;
	}

}
