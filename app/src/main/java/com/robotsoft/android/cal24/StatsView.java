package com.robotsoft.android.cal24;

import android.app.Activity;
import android.app.ListActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class StatsView extends ListActivity {
	
	protected void onCreate(Bundle savedInstanceState) {
		try {
			super.onCreate(savedInstanceState);
			StatsItem[] items=Util.getStats(this);
			setContentView(R.layout.statslist);
			ListView view = getListView();
			view.setItemsCanFocus(false);
			StatsItemAdapter adapter = new StatsItemAdapter(this, items);
			setListAdapter(adapter);
			
		} catch (Throwable e) {
			Log.e("Cat24", "Create Spot List Error", e);
		}
	}


	public void doClear(View v) {
		Util.clearStats(this);
		finish();
	}
	
	public void doExit(View v) {
		finish();
	}
	
	class StatsItemAdapter extends ArrayAdapter<StatsItem> {
		public StatsItemAdapter(Activity context,StatsItem[] list) {
			super(context, R.layout.stats_list_item, list);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			StatsItem s = getItem(position);
			LayoutInflater inflater = ((Activity) getContext()).getLayoutInflater();
			View v=inflater.inflate(R.layout.stats_list_item, null, true);
			TextView txt=(TextView)v.findViewById(R.id.score);
			txt.setText(String.valueOf(s.getScore()));
			txt=(TextView)v.findViewById(R.id.time);
			int t=s.getTimeSpane();
			if(t<60) {
				txt.setText(String.valueOf(t)+" sec");
			}
			else {
				txt.setText(String.valueOf(t/60)+" min");
			}
			txt=(TextView)v.findViewById(R.id.card);
			txt.setText("#"+String.valueOf(s.getCardNumber()));
			txt=(TextView)v.findViewById(R.id.repeat);
			txt.setText(s.getCount()==1?"":String.valueOf(s.getCount())+" times");
			ImageView img=(ImageView)v.findViewById(R.id.star);
			img.setImageDrawable(getResources().getDrawable(
					s.isAdvanced() ? android.R.drawable.star_on
							: android.R.drawable.star_off));
			return v;
		}
	}

}
