package com.robotsoft.android.cal24;

public class StatsItem {
	private int timeSpane;
	private int cardNumber;
	private int score;
	private boolean advanced;
	private int count;
	
	public StatsItem(int timeSpane, int cardNumber, int score, boolean advanced) {
		this.timeSpane = timeSpane;
		this.cardNumber = cardNumber;
		this.score = score;
		this.advanced=advanced;
		count=1;
	}
	public int getTimeSpane() {
		return timeSpane;
	}
	public void setTimeSpane(int timeSpane) {
		this.timeSpane = timeSpane;
	}
	public int getCardNumber() {
		return cardNumber;
	}
	public void setCardNumber(int cardNumber) {
		this.cardNumber = cardNumber;
	}
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}
	public boolean isAdvanced() {
		return advanced;
	}
	
	public void addCount() {
		count++;
	}
	public int getCount() {
		return count;
	}

}
