package com.robotsoft.android.cal24;

public interface DBConstants {
	
	public static final String DataAutority="com.robotsoft.android.cal24.provider";
	public static final String StatsTable = "recording";
	public static final String CardNumber = "card_number";
	public static final String TimeSpan = "time_span";
	public static final String Score = "score";
	public static final String Advanced = "advanced";
	

}
