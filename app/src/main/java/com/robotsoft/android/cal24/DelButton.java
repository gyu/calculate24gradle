package com.robotsoft.android.cal24;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Paint.Style;
import android.util.AttributeSet;

public class DelButton extends ButtonExt {
	private float tip;

	public DelButton(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public DelButton(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public DelButton(Context context) {
		super(context);
	}
	
	protected void init(Context context) {
		super.init(context);
		float paddingScale = context.getResources().getDisplayMetrics().density;
		tip = 4.5f* paddingScale;
	}

	
	protected void doDraw(Canvas canvas) {
		int w=this.getWidth();
		int h=this.getHeight();
		Paint p1=getPaint();
		Paint p=new Paint();
		p.setColor(p1.getColor());
		p.setAntiAlias(true);
		p.setStyle(Style.STROKE);
		p.setStrokeWidth(tip);
		int r=Math.min(w,h)/4;
		canvas.drawCircle(w/2, h/2, r, p);
		r=r/2;
		canvas.drawLine(w/2-r, h/2-r, w/2+r, h/2+r,p);
		canvas.drawLine(w/2+r, h/2-r, w/2-r, h/2+r,p);
	}

}
