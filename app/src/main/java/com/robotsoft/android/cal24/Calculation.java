package com.robotsoft.android.cal24;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import android.util.Log;

public class Calculation {
	private static final String TheSign = "+-*/";
	private static final String SignAlt = "+\u2212\u00d7\u00f7";
	private static final String UsingChars = "+-*/()0123456789";
	
	private static int Count;
	private List<int[]> permutations(int[] w) {
		int n = w.length;
		List<int[]> res = new ArrayList<int[]>();
		if (n == 1) {
			res.add(w);
		} else {
			int[] u = new int[n - 1];
			for (int i = 0; i < n; i++) {
				for (int j = 0, k = 0; j < n; j++) {
					if (i != j)
						u[k++] = w[j];
				}
				List<int[]> z = permutations(u);
				L1: for (int[] q : z) {
					L2: for (int[] r : res) {
						if (r[0] == w[i]) {
							for (int j = 0; j < n - 1; j++) {
								if (r[j + 1] != q[j]) {
									continue L2;
								}
							}
							continue L1;
						}
					}
					int[] s = new int[n];
					s[0] = w[i];
					System.arraycopy(q, 0, s, 1, n - 1);
					res.add(s);
				}
			}
		}
		return res;
	}

	private void doCal(List<Val> res, Val a, Val b) {
		res.add(new Val(a, b, a.value + b.value, 0));
		res.add(new Val(a, b, a.value - b.value, 1));
		res.add(new Val(a, b, a.value * b.value, 2));
		if (b.value != 0) {
			res.add(new Val(a, b, a.value / b.value, 3));
		}
	}

	private List<Val> calculate(Val[] q) {
		List<Val> res = new ArrayList<Val>();
		int n = q.length;
		if (n == 2) {
			doCal(res, q[0], q[1]);
		} else {
			Val[] r = new Val[n - 1];
			System.arraycopy(q, 1, r, 0, n - 1);
			for (Val v : calculate(r)) {
				doCal(res, q[0], v);
			}
			for (int i = 2; i < n - 1; i++) {
				Val[] c = new Val[i];
				Val[] d = new Val[n - i];
				System.arraycopy(q, 0, c, 0, i);
				System.arraycopy(q, i, d, 0, n - i);
				for (Val a : calculate(c)) {
					for (Val b : calculate(d)) {
						doCal(res, a, b);
					}
				}
			}
			System.arraycopy(q, 0, r, 0, n - 1);
			for (Val v : calculate(r)) {
				doCal(res, v, q[n - 1]);
			}
		}
		return res;
	}

	public static double getValue(String equation, int[] cards) throws SyntaxException {
		ManyItem im = toItem(equation);
		if(!im.formatValid()) {
			throw new SyntaxException(R.string.operator_error);
		}
		List<Integer> res=im.extract();
		int n=cards.length;
		if(res.size()!=n) {
			throw new SyntaxException(R.string.number_error);
		}
		Collections.sort(res);
		for(int i=0; i<n; i++) {
			if(res.get(i)!=cards[i]) {
				throw new SyntaxException(R.string.number_error);
			}
		}
		return im.getValue();
	}

	
	public static boolean eval(String equation, int[] cards) throws SyntaxException {
		equation=equation.replace(SignAlt.charAt(1), '-');
		equation=equation.replace(SignAlt.charAt(2), '*');
		equation=equation.replace(SignAlt.charAt(3), '/');
		char[] cs=equation.toCharArray();
		for(char c:cs) {
			if(UsingChars.indexOf(c)<0) {
				throw new SyntaxException(R.string.invalid_chars);
			}
		}
		Arrays.sort(cards);
		//equation="(1+13/13)*12";
		//cards=new int[]{1,12,13,13};
		Log.d("Cal24","Eavaluate  "+equation);
		double d=getValue(equation, cards);
		Log.d("Cal24","Result is "+d+"   "+(Math.abs(d-24)<1e-7)+"  "+Math.abs(d-24));
		return Math.abs(d-24)<1e-7;
	}

	public List<String> process(int[] vals) {
		List<Val> res = new ArrayList<Val>();
		int n = vals.length;
		int ss=0;
		for (int[] a : permutations(vals)) {
			ss++;
			Val[] q = new Val[n];
			for (int i = 0; i < n; i++) {
				q[i] = new Val(a[i]);
			}
			res.addAll(calculate(q));
		}
		Count = 0;
		List<String> lst=new ArrayList<String>();
		Set<String> set=new HashSet<String>();
		
		for (Val z : res) {
			++Count;
			if (Math.abs(z.value - 24) < 1e-6) {
				String s = z.toString();
				if (!set.contains(s)) {
					set.add(s);
					lst.add(s.replace('-',SignAlt.charAt(1)).replace('*', SignAlt.charAt(2)).replace('/', SignAlt.charAt(3)));
				}
			}
		}
		return lst;
	}


	private static ManyItem toItem(String s) throws SyntaxException {
		List<Item> list = new ArrayList<Item>();
		char[] cs = s.toCharArray();
		int j = -1;
		int k = 0;
		StringBuffer buf = new StringBuffer();
		for (int i = 0, n = cs.length; i < n; i++) {
			switch (cs[i]) {
			case '(':
				if (j == -1) {
					k = 1;
					j = i;

				} else {
					k++;
				}
				break;
			case ')':
				if(j==-1) {
					throw new SyntaxException(R.string.brace_error);
				}
				k--;
				if (k == 0) {
					list.add(toItem(s.substring(j + 1, i)));
					j = -1;
				}
				break;
			case '*':
			case '/':
			case '+':
			case '-':
				if (j == -1) {
					if (buf.length() > 0) {
						list.add(new ValueItem(Integer.parseInt(buf.toString())));
						buf.setLength(0);
					}
					list.add(new SignItem(cs[i]));
				}
				break;
			default:
				if (j == -1) {
					buf.append(cs[i]);

				}
			}
		}
		if(j!=-1) {
			throw new SyntaxException(R.string.brace_error);
		}
		if (buf.length() > 0) {
			list.add(new ValueItem(Integer.parseInt(buf.toString())));
		}
		return new ManyItem(list);
	}

	private static class ValComparator implements Comparator<Val> {

		@Override
		public int compare(Val v1, Val v2) {
			if (v1.theValue() < v2.theValue())
				return -1;
			if (v1.theValue() > v2.theValue())
				return 1;
			return 0;
		}

	}

	private static class Val {
		private Val a;
		private Val b;
		private double value;
		private int operator;
		private boolean negative=false;

		Val(double v) {
			value = v;
			operator = -1;
		}

		public Val(Val a, Val b, double value, int operator) {
			super();
			doSet( a,  b, value, operator);
		}
		
		
		private void doSet(Val a, Val b, double value, int operator) {
			
			this.a = a;
			this.value = value;
			if (operator == 1 && b.value < 0 && b.operator == 1) {
				this.b = new Val(b.b, b.a, -b.value, 1);
				this.operator = 0;
			} else if (operator == 1 && b.value < 0 && b.operator > 1) {
				if (b.a.value < 0) {
					Val c = new Val(b.a.b, b.a.a, -b.a.value, 1);
					this.b = new Val(c, b.b, -b.value, b.operator);
					this.operator = 0;
				} else if (b.b.value < 0) {
					Val c = new Val(b.b.b, b.b.a, -b.b.value, 1);
					this.b = new Val(b.a, c, -b.value, b.operator);
					this.operator = 0;
				} else {
					this.b = b;
					this.operator = operator;
				}
			} else {
				this.b = b;
				this.operator = operator;
			}
		}
		
		double theValue() {
			if(negative) return -value;
			return value;
		}

		boolean simple() {
			return operator == -1;
		}

		int[] toInt(boolean positive) {
			if (simple()) {
				int i = (int) Math.round(value);
				if (!positive)
					i = -i;
				return new int[] { i };
			}
			int[] a1 = a.toInt(positive);
			int[] a2 = b.toInt(positive == (operator != 1));
			int[] res = new int[a1.length + a2.length];
			System.arraycopy(a1, 0, res, 0, a1.length);
			System.arraycopy(a2, 0, res, a1.length, a2.length);
			return res;
		}

		int count() {
			if (simple())
				return 1;
			return a.count() + b.count();
		}

		boolean sameOp(int op) {
			if (simple())
				return true;
			if (operator != op)
				return false;
			return a.sameOp(op) && b.sameOp(op);
		}

		boolean addition() {
			if (simple())
				return true;
			if (operator >= 2)
				return false;
			return a.addition() && b.addition();
		}

		public String toString() {
			if (simple())
				return String.valueOf(Math.round(value));
			StringBuffer buf = new StringBuffer();
			boolean dbg = (Count == 291 && count() == 4);

			if (count() == 4 && sameOp(2)) {
				int[] vals = toInt(true);
				Arrays.sort(vals);
				for (int i = 3; i >= 0; i--) {
					if (i < 3) {
						buf.append("*");
					}
					buf.append(vals[i]);
				}
				return buf.toString();
			}
			if (count() == 4 && addition()) {
				int[] vals = toInt(true);
				Arrays.sort(vals);
				for (int i = 3; i >= 0; i--) {
					if (i < 3 && vals[i] > 0) {
						buf.append("+");
					}
					buf.append(vals[i]);
				}
				return buf.toString();
			}

			if (operator >= 2
					&& ((a.simple() && !b.simple() && b.operator >= 2) || (b
							.simple() && !a.simple() && a.operator >= 2))) {
				if (count() == 3) {
					if (!a.simple() && (operator == 2 || a.operator == 2)) {
						if (operator == 2 && a.operator == 2) {
							int[] vals = toInt(true);
							Arrays.sort(vals);
							for (int i = 2; i >= 0; i--) {
								if (i < 2) {
									buf.append("*");
								}
								buf.append(vals[i]);
							}
						} else if (operator == 3) {
							buf.append((a.a.value > a.b.value) ? a.a.toString()
									: a.b.toString());
							buf.append("*").append(
									(a.a.value > a.b.value) ? a.b.toString()
											: a.a.toString());
							buf.append("/").append(b.toString());
						} else {
							buf.append((a.a.value > b.value) ? a.a.toString()
									: b.toString());
							buf.append("*").append(
									(a.a.value > b.value) ? b.toString() : a.a
											.toString());
							buf.append("/").append(a.b.toString());
						}
						return buf.toString();
					} else if (operator == 2 && !b.simple()) {
						if (b.operator == 2) {
							int[] vals = toInt(true);
							Arrays.sort(vals);
							for (int i = 2; i >= 0; i--) {
								if (i < 2) {
									buf.append("*");
								}
								buf.append(vals[i]);
							}
						} else {
							buf.append((a.value > b.a.value) ? a.toString()
									: b.a.toString());
							buf.append("*").append(
									(a.value > b.a.value) ? b.a.toString() : a
											.toString());
							buf.append("/").append(b.b.toString());
						}
						return buf.toString();
					}
				}
			}

			if (operator < 2
					&& ((a.simple() && !b.simple() && b.operator < 2) || (b
							.simple() && !a.simple() && a.operator < 2))) {
				boolean typeOne = false;
				if (!a.simple()) {
					if ((!a.a.simple() && a.a.operator > 1)
							|| (!a.b.simple() && a.b.operator > 1)) {
						typeOne = true;
					}
				} else if (!b.simple()) {
					if ((!b.a.simple() && b.a.operator > 1)
							|| (!b.b.simple() && b.b.operator > 1)) {
						typeOne = true;
					}
				}
				if (typeOne) {
					Val[] vals = new Val[3];
					if (a.simple()) {
						vals[0] = a;
						vals[1] = b.a;
						vals[1].negative=operator==1;
						vals[2] = b.b;
						vals[2].negative=(operator==1)!=(b.operator==1);
					} else {
						vals[0] = a.a;
						vals[1] = a.b;
						vals[1].negative=a.operator==1;
						vals[2] = b;
						vals[2].negative=operator==1;
					}
					Arrays.sort(vals, new ValComparator());
					for (int i = 2; i >= 0; i--) {
						if (i < 2) {
							if (vals[i].negative)
								buf.append("-");
							else
								buf.append("+");
						}
						buf.append(vals[i].toString());
					}
					return buf.toString();
				}
				int[] vals = toInt(true);
				if (vals.length == 4) {
					Arrays.sort(vals);
					buf.append(vals[3]);
					if (vals[2] > 0)
						buf.append("+");
					buf.append(vals[2]);
					if (vals[1] > 0)
						buf.append("+");
					buf.append(vals[1]);
					if (vals[0] > 0)
						buf.append("+");
					buf.append(vals[0]);
					return buf.toString();
				}
			}
			if (!a.simple() && !b.simple()) {
				if (operator <= 1 && a.operator <= 1 && b.operator <= 1) {
					int[] vals = new int[4];
					vals[0] = (int) Math.round(a.a.value);
					vals[1] = (int) Math.round(a.b.value);
					if (a.operator == 1)
						vals[1] = -vals[1];
					vals[2] = (int) Math.round(b.a.value);
					if (operator == 1)
						vals[2] = -vals[2];
					vals[3] = (int) Math.round(b.b.value);
					if (b.operator != operator)
						vals[3] = -vals[3];
					Arrays.sort(vals);
					buf.append(vals[3]);
					if (vals[2] > 0)
						buf.append("+");
					buf.append(vals[2]);
					if (vals[1] > 0)
						buf.append("+");
					buf.append(vals[1]);
					if (vals[0] > 0)
						buf.append("+");
					buf.append(vals[0]);
					return buf.toString();
				}
			}
			Val aa;
			Val bb;
			if ((operator == 0 || operator == 2)
					&& ((a.value > 0 && b.value > 0 && b.value > a.value) || (a.value < 0
							&& b.value < 0 && b.value < a.value))) {
				aa = b;
				bb = a;
			} else {
				aa = a;
				bb = b;
			}
			if (aa.value < 0 && bb.value < 0 && aa.operator == 1
					&& bb.operator == 1) {
				buf.append('(').append(aa.b.toString()).append('-')
						.append(aa.a.toString()).append(")*(")
						.append(bb.b.toString()).append('-')
						.append(bb.a.toString()).append(')');
				return buf.toString();
			}
			if (dbg)
				System.out.println("aaaaaaaaaaaaa");
			if (operator <= 1 && aa.simple() != bb.simple()) {
				if ((aa.simple() && bb.operator <= 1 && bb.a.simple() && bb.b
						.simple())
						|| (bb.simple() && aa.operator <= 1 && aa.a.simple() && aa.b
								.simple())) {
					int[] w = new int[3];
					if (aa.simple()) {
						w[0] = (int) Math.round(aa.value);
						w[1] = (int) Math.round(bb.a.value);
						if (operator == 1)
							w[1] = -w[1];
						w[2] = (int) Math.round(bb.b.value);
						if (operator != bb.operator)
							w[2] = -w[2];
					} else {
						w[0] = (int) Math.round(aa.a.value);
						w[1] = (int) Math.round(aa.b.value);
						if (aa.operator == 1)
							w[1] = -w[1];
						w[2] = (int) Math.round(bb.value);
						if (operator == 1)
							w[2] = -w[2];
					}
					Arrays.sort(w);
					buf.append(w[2]);
					if (w[1] > 0)
						buf.append("+");
					buf.append(w[1]);
					if (w[0] > 0)
						buf.append("+");
					buf.append(w[0]);
					return buf.toString();
				}
			}
			if (aa.simple() || operator < 2 || aa.operator == 2) {
				buf.append(aa.toString());
			} else {
				buf.append('(').append(aa.toString()).append(')');
			}
			buf.append(TheSign.charAt(operator));
			if (bb.simple() || (operator == 2 && bb.operator >= 2)
					|| (bb.operator == 2 && operator != 3)
					|| (bb.operator == 3 && operator < 2)) {
				buf.append(bb.toString());
			} else {
				buf.append('(').append(bb.toString()).append(')');
			}
			return buf.toString();
		}

	}

	private static class Item {
	}

	private static class SignItem extends Item {
		char operator;

		public SignItem(char operator) {
			super();
			this.operator = operator;
		}

	}

	private static class ValueItem extends Item {
		int value;

		public ValueItem(int value) {
			super();
			this.value = value;
		}
	}

	private static class DoubleItem extends Item {
		double value;

		public DoubleItem(double value) {
			super();
			this.value = value;
		}
	}

	private static class ManyItem extends Item {
		List<Item> list;

		ManyItem(List<Item> list) {
			this.list = list;
		}

		public boolean formatValid() {
			boolean sign=false;
			for(int i=0,n=list.size(); i<n; i++) {
				Item m=list.get(i);
				if(m instanceof ManyItem) {
					if(!((ManyItem)m).formatValid()) {
						return false;
					}
					if(i>0&&!sign) return false;
					sign=false;
				}
				else if(m instanceof ValueItem) {
					if(i>0&&!sign) return false;
					sign=false;
				}
				else  {
					SignItem sm=(SignItem)m;
					char c=sm.operator;
					if(i==0&&(c=='*'||c=='/'))return false;
					if(i>0&&sign) return false;
					sign=true;
				}
			}
			return true;
		}
		
		List<Integer> extract() {
			List<Integer> res=new ArrayList<Integer>();
			for (Item i : list) {
				if (i instanceof ManyItem) {
					res.addAll(((ManyItem) i).extract());
				} else if (i instanceof ValueItem){
					res.add(((ValueItem) i).value);
				}
			}
			return res;
		}

		double getValue() {
			List<Item> im = new ArrayList<Item>();
			double current = 0;
			char last = 0;
			for (Item i : list) {
				if (i instanceof SignItem) {
					char c = ((SignItem) i).operator;
					if (c == '+' || c == '-') {
						im.add(new DoubleItem(current));
						im.add(i);
						last = 0;
					} else
						last = c;
				} else {
					double v;
					if (i instanceof ManyItem) {
						v = ((ManyItem) i).getValue();
					} else {
						v = ((ValueItem) i).value;
					}
					if (last == 0) {
						current = v;
					} else if (last == '*') {
						current *= v;
					} else {
						current /= v;
					}

				}
			}
			im.add(new DoubleItem(current));
			current = 0;
			last = 0;
			for (Item i : im) {
				if (i instanceof SignItem) {
					last = ((SignItem) i).operator;
				} else {
					double v = ((DoubleItem) i).value;
					if (last == 0) {
						current = v;
					} else if (last == '+') {
						current += v;
					} else {
						current -= v;
					}
				}
			}
			return current;
		}

	}

}
