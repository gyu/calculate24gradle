package com.robotsoft.android.cal24;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Paint.Style;
import android.util.AttributeSet;

public class BackButton extends ButtonExt {
	private int tip2;

	public BackButton(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public BackButton(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public BackButton(Context context) {
		super(context);
	}
	
	protected void init(Context context) {
		super.init(context);
		float paddingScale = context.getResources().getDisplayMetrics().density;
		tip2 = (int) (2* paddingScale + 0.5);
	}
	
	protected void doDraw(Canvas canvas) {
		int w=this.getWidth();
		int h=this.getHeight();
		Paint p1=getPaint();
		Paint p=new Paint();
		p.setColor(p1.getColor());
		p.setAntiAlias(true);
		p.setStyle(Style.FILL);
		Path t = new Path();
		int thick=w*3/16;
		t.moveTo( w /4 +thick,  h / 2 - thick*2/3);
		t.lineTo( w/4,  h / 2);
		t.lineTo( w / 4+ thick,  h / 2 + thick*2/3);
		canvas.drawPath(t, p);
		canvas.drawRect( w /4 +thick,  h / 2 - tip2,  w *3/ 4, h / 2+ tip2, p);
	}

}
