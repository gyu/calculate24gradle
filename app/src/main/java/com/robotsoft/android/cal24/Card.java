package com.robotsoft.android.cal24;


public enum Card {
	SA(Suit.Spade,Rank.A,R.drawable.s_a),
	SK(Suit.Spade,Rank.K,R.drawable.s_k),
	SQ(Suit.Spade,Rank.Q,R.drawable.s_q),
	SJ(Suit.Spade,Rank.J,R.drawable.s_j),
	S10(Suit.Spade,Rank.N10,R.drawable.s_10),
	S9(Suit.Spade,Rank.N9,R.drawable.s_9),
	S8(Suit.Spade,Rank.N8,R.drawable.s_8),
	S7(Suit.Spade,Rank.N7,R.drawable.s_7),
	S6(Suit.Spade,Rank.N6,R.drawable.s_6),
	S5(Suit.Spade,Rank.N5,R.drawable.s_5),
	S4(Suit.Spade,Rank.N4,R.drawable.s_4),
	S3(Suit.Spade,Rank.N3,R.drawable.s_3),
	S2(Suit.Spade,Rank.N2,R.drawable.s_2),
	HA(Suit.Heart,Rank.A,R.drawable.h_a),
	HK(Suit.Heart,Rank.K,R.drawable.h_k),
	HQ(Suit.Heart,Rank.Q,R.drawable.h_q),
	HJ(Suit.Heart,Rank.J,R.drawable.h_j),
	H10(Suit.Heart,Rank.N10,R.drawable.h_10),
	H9(Suit.Heart,Rank.N9,R.drawable.h_9),
	H8(Suit.Heart,Rank.N8,R.drawable.h_8),
	H7(Suit.Heart,Rank.N7,R.drawable.h_7),
	H6(Suit.Heart,Rank.N6,R.drawable.h_6),
	H5(Suit.Heart,Rank.N5,R.drawable.h_5),
	H4(Suit.Heart,Rank.N4,R.drawable.h_4),
	H3(Suit.Heart,Rank.N3,R.drawable.h_3),
	H2(Suit.Heart,Rank.N2,R.drawable.h_2),
	DA(Suit.Diamond,Rank.A,R.drawable.d_a),
	DK(Suit.Diamond,Rank.K,R.drawable.d_k),
	DQ(Suit.Diamond,Rank.Q,R.drawable.d_q),
	DJ(Suit.Diamond,Rank.J,R.drawable.d_j),
	D10(Suit.Diamond,Rank.N10,R.drawable.d_10),
	D9(Suit.Diamond,Rank.N9,R.drawable.d_9),
	D8(Suit.Diamond,Rank.N8,R.drawable.d_8),
	D7(Suit.Diamond,Rank.N7,R.drawable.d_7),
	D6(Suit.Diamond,Rank.N6,R.drawable.d_6),
	D5(Suit.Diamond,Rank.N5,R.drawable.d_5),
	D4(Suit.Diamond,Rank.N4,R.drawable.d_4),
	D3(Suit.Diamond,Rank.N3,R.drawable.d_3),
	D2(Suit.Diamond,Rank.N2,R.drawable.d_2),
	CA(Suit.Club,Rank.A,R.drawable.c_a),
	CK(Suit.Club,Rank.K,R.drawable.c_k),
	CQ(Suit.Club,Rank.Q,R.drawable.c_q),
	CJ(Suit.Club,Rank.J,R.drawable.c_j),
	C10(Suit.Club,Rank.N10,R.drawable.c_10),
	C9(Suit.Club,Rank.N9,R.drawable.c_9),
	C8(Suit.Club,Rank.N8,R.drawable.c_8),
	C7(Suit.Club,Rank.N7,R.drawable.c_7),
	C6(Suit.Club,Rank.N6,R.drawable.c_6),
	C5(Suit.Club,Rank.N5,R.drawable.c_5),
	C4(Suit.Club,Rank.N4,R.drawable.c_4),
	C3(Suit.Club,Rank.N3,R.drawable.c_3),
	C2(Suit.Club,Rank.N2,R.drawable.c_2);
	
	private Suit suit;
	private Rank rank;
	private int value;
	private int resId;
	
	private Card(Suit suit, Rank rank, int resId) {
		this.suit = suit;
		this.rank = rank;
		int k=rank.ordinal();
		this.value = k==0?1:14-k;
		this.resId = resId;
	}

	public Suit getSuit() {
		return suit;
	}

	public Rank getRank() {
		return rank;
	}

	public int getValue() {
		return value;
	}

	public int getResId() {
		return resId;
	}

}
