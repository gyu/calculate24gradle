package com.robotsoft.android.cal24;

import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface.OnClickListener;
import android.content.SharedPreferences;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

public class DeckPreference extends DialogPreference implements View.OnClickListener {
	private ImageView[] imgs=new ImageView[4]; 
	private int current;
	
	private static int[] ImageRef={R.id.first,R.id.second,R.id.third,R.id.fourth};
	private static int[][] SourceRef={{R.drawable.deck1,R.drawable.deck1_sel},
		{R.drawable.deck2,R.drawable.deck2_sel},
		{R.drawable.deck3,R.drawable.deck3_sel},
		{R.drawable.deck4,R.drawable.deck4_sel}};
	
	public DeckPreference(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	public static int getResourceId(SharedPreferences prefs) {
		int inx=prefs.getInt(CardPref.Deck, 3);
		return SourceRef[inx][0];
	}


	@Override
	protected void onDialogClosed(boolean positiveResult) {
		persistInt(current);
		
		super.onDialogClosed(positiveResult);
	}
	
	@Override
	protected void onPrepareDialogBuilder(Builder builder) {
		LayoutInflater vi = (LayoutInflater) getContext().getSystemService(
				Context.LAYOUT_INFLATER_SERVICE);
		current=getSharedPreferences().getInt(CardPref.Deck, 3);
		View v=vi.inflate(R.layout.decks, null);
		builder.setView(v);
		for(int i=0; i<4; i++) {
			imgs[i]=(ImageView)v.findViewById(ImageRef[i]);
			imgs[i].setOnClickListener(this);
		}
		imgs[current].setImageDrawable(getContext().getResources().getDrawable(SourceRef[current][1]));
		
	}
	
	@Override
	public void onClick(View v) {
		for(int i=0; i<4; i++) {
			if(v.equals(imgs[i])) {
				if(i!=current) {
					imgs[current].setImageDrawable(getContext().getResources().getDrawable(SourceRef[current][0]));
					current=i;
					imgs[current].setImageDrawable(getContext().getResources().getDrawable(SourceRef[current][1]));
				}
				break;
			}
		}
	}
}
