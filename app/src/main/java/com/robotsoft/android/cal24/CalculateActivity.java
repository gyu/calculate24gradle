package com.robotsoft.android.cal24;

import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PixelFormat;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.util.TypedValue;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class CalculateActivity extends Activity {
	public static final String[] Names = { "A", "2", "3", "4", "5", "6", "7",
			"8", "9", "10", "J", "Q", "K" };
	private EventListener listener = new EventListener();
	private MainView view;
	private ClockView clock;
	private EditText input;
	private PanelSwitcher panelSwitcher;
	private ButtonExt[] buttons = new ButtonExt[4];

	static final int BASIC_PANEL = 0;
	static final int ADVANCED_PANEL = 1;
	private static final int HVGA_WIDTH_PIXELS = 320;
	private TextView textLabel;
	private TextView textContent;
	private TextView newScore;
	private TextView totalScore;
	private Button newPlay;
	private Button nextSet;
	private Button viewMore;

	private String[] solutions;
	private boolean started = false;
	private boolean done = true;
	private Handler handler;
	private ProgressDialog dialog;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.main);
		handler = new Handler();
		Log.d("Cal24", "@@@ Start Applocation");
		try {
			SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
			Media.setPlaying(prefs.getBoolean(CardPref.Sound, true));
			View v=findViewById(R.id.main_frame);
			view = (MainView) v;
			input = (EditText) findViewById(R.id.display);
			panelSwitcher = (PanelSwitcher) findViewById(R.id.panelswitch);
			panelSwitcher.setCurrentIndex(0);
			input.setOnKeyListener(listener);
			buttons[0] = (ButtonExt) findViewById(R.id.first);
			buttons[1] = (ButtonExt) findViewById(R.id.second);
			buttons[2] = (ButtonExt) findViewById(R.id.third);
			buttons[3] = (ButtonExt) findViewById(R.id.fourth);
			textLabel = (TextView) findViewById(R.id.text_label);
			textContent = (TextView) findViewById(R.id.text_content);
			newScore = (TextView) findViewById(R.id.score_new);
			totalScore = (TextView) findViewById(R.id.score_total);
			newPlay = (Button) findViewById(R.id.new_play);
			nextSet = (Button) findViewById(R.id.next);
			viewMore = (Button) findViewById(R.id.review);
			clock = (ClockView) findViewById(R.id.clock);
			listener.setHost(this, input, clock);
			registerForContextMenu(view);
			InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(input.getWindowToken(), 0);
			if(clock==null) {
				Log.e("Cal24","Fatal error in inflating the main XML");
				throw new RuntimeException("Fatal error in inflating the main XML");
			}
			Log.d("Cal24", "End Init Applocation");
			restart();
		} catch (Throwable e) {
			Log.e("Cal24", "Some Fatal error?", e);
		}
	}
	
	@Override
	public void onAttachedToWindow() {
		super.onAttachedToWindow();
		Window window=getWindow();
		window.setFormat(PixelFormat.RGBA_8888);
	}

	public void restart() {
		started = false;
		listener.reset(this);
		clock.reset();
		viewMore.setVisibility(View.GONE);
		nextSet.setVisibility(View.VISIBLE);
		nextSet.setText(R.string.start);
		newPlay.setVisibility(View.GONE);
		textLabel.setVisibility(View.GONE);
		textContent.setText(getString(R.string.start_new_play));
		newScore.setTextColor(0xff00001f);
		newScore.setText("0");
		totalScore.setText("0");
		newPlay.setEnabled(false);
		nextSet.setEnabled(false);
		viewMore.setEnabled(false);
		handler.postDelayed(new StartAnimator(), 100);
		Media.start(this, R.raw.mickey);
	}

	public void viewMore(View view) {
		if (solutions != null) {
			Log.d("Cal24", "Show More: " + solutions.length + "   "
					+ getString(R.string.show_solutions, solutions.length));
			try {
				
				AlertDialog.Builder alert = new AlertDialog.Builder(this);
				alert.setIcon(android.R.drawable.ic_dialog_info).setTitle(R.string.solutions).setPositiveButton(R.string.ok_action, null);
				alert.setItems(solutions, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int item) {
					}
				});
				alert.show();
			} catch (Throwable e) {
				Log.e("Cal24", "Dialog Error", e);
			}
		}
	}

	public void goNext(View v) {
		if (!started) {
			started = true;
			nextSet.setText(R.string.next);
			newPlay.setVisibility(View.VISIBLE);
		}
		int[] cards = view.pickNext();
		int n = 0;
		for (int i = 0; i < 4; i++) {
			boolean b=true;
			for(int j=0; j<i; j++) {
				if(cards[j]==cards[i]) {
					b=false;
					break;
				}
			}
			if(b) {
				buttons[n].setVisibility(View.VISIBLE);
				buttons[n++].setText(Names[cards[i] - 1]);
			}
		}
		for(int i=n; i<4; i++) {
			buttons[i].setVisibility(View.GONE);
		}
		int[] u = new int[4];
		boolean b=listener.isAdvanced();
		for(int i=0; i<4; i++) {
			u[i]=(b||cards[i]<11)?cards[i]:1;
		}
		Media.start(this, R.raw.mickey);
		doFetch(u);
	}

	public void newPlay(View view) {
		restart();
	}

	public EventListener getListener() {
		return listener;
	}

	public void adjustFontSize(TextView view) {
		float fontPixelSize = view.getTextSize();
		Display display = getWindowManager().getDefaultDisplay();
		int h = Math.min(display.getWidth(), display.getHeight());
		float ratio = (float) h / HVGA_WIDTH_PIXELS;
		view.setTextSize(TypedValue.COMPLEX_UNIT_PX, fontPixelSize * ratio);
	}

	private void addScore(int score) {
		view.changeCard(score);
		viewMore.setVisibility(score != 0 ? View.VISIBLE : View.GONE);
		textLabel.setVisibility(score == 0 ? View.GONE : View.VISIBLE);
		textContent.setText(solutions == null || score == 0 ? getString(R.string.no_solution)
						: score < 0 ? solutions[0] : input.getText().toString());
		newScore.setTextColor(score < 0 ? 0xffff0000 : score > 0 ? 0xff00ff00
				: 0xff00001f);
		newScore.setText(String.valueOf(score));
		if (score != 0) {
			String s = totalScore.getText().toString();
			int v;
			if (s.length() == 0) {
				v = 0;
			} else {
				v = Integer.parseInt(s);
			}
			v += score;
			totalScore.setText(String.valueOf(v));
		}
		panelSwitcher.goRight();
	}

	public void doSubmit() {
		Media.step(this);
		String s = input.getText().toString();
		int[] cards = view.getCards();
		if(!listener.isAdvanced()) {
			for(int i=0; i<4; i++) {
				if(cards[i]>=11) {
					cards[i]=1;
				}
			}
		}
		try {
			if (!Calculation.eval(s, cards)) {
				Toast.makeText(this, R.string.wrong_value, Toast.LENGTH_LONG).show();
			} else {
				clock.stop();
				addScore(clock.getScore(true));
				if(view.getStatus()==1) {
					gameOver(true);
				}
			}
		} catch (SyntaxException ex) {
			Toast.makeText(this, ex.getMessage(this), Toast.LENGTH_LONG).show();
		}
	}

	public void doGiveup() {
		Media.step(this);
		lostTheSet(false);
	}
	
	private void gameOver(boolean win) {
		
		nextSet.setVisibility(View.GONE);
		newPlay.setVisibility(View.VISIBLE);
		viewMore.setVisibility(View.GONE);
		LayoutInflater vi = (LayoutInflater) getSystemService(
				Context.LAYOUT_INFLATER_SERVICE);
		int resId=win?R.layout.congratulation:R.layout.consolidation;
		View v = vi.inflate(resId, null);
		if(win) {
			Media.congratulate(this);
			int t=Integer.parseInt(totalScore.getText().toString());
			TextView txt=(TextView)v.findViewById(R.id.score);
			txt.setText(String.valueOf(t));
			Util.addStats(this, new StatsItem(clock.getTimeSpan()/1000,view.getTotalCards(),t,listener.isAdvanced()));
		}
		else {
			Media.feelSorry(this);
		}
		final Toast toast = new Toast(this);
		toast.setView(v);
		toast.setDuration(Toast.LENGTH_LONG);
		toast.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.TOP, 0, 100);
		toast.show();
		handler.postDelayed(new Runnable() {
			@Override
			public void run() {
				toast.cancel();
				openContextMenu(view);
			}
		}, 5000);

	}

	public void expire() {
		Media.step(this);
		lostTheSet(true);
	}
	
	
	private void lostTheSet(boolean show) {
		clock.stop();
		addScore(-clock.getScore(false));
		if(view.getStatus()==-1) {
			gameOver(false);
		}
		else if(show){
			Toast.makeText(this, R.string.exceed_time, Toast.LENGTH_LONG).show();
		}
	}

	private void setDone() {
		done = true;
	}

	private boolean isDone() {
		return done;
	}

	private void doFetch(int[] cards) {
		try {
			newPlay.setEnabled(false);
			nextSet.setEnabled(false);
			viewMore.setEnabled(false);
			textContent.setText("");
			newScore.setText("");
			input.setText("");
			done = false;
			FetchTask ft=new FetchTask();
			ft.execute(cards);
			handler.postDelayed(new PickAnimator(true), 100);
		} catch (Throwable e) {
			Log.e("Cal24", "Error Fetch.....", e);
		}
	}
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		menu.setHeaderTitle(getResources().getString(R.string.main_context));
		getMenuInflater().inflate(R.menu.context_menu, menu);
	}

	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		getMenuInflater().inflate(R.menu.main_option, menu);
		return true;
	}

	
	@Override
	public void onBackPressed() {
		openContextMenu(view);
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		Intent i=null;
		switch (item.getItemId()) {
		case R.id.exit:finish(); return true;
		case R.id.contnue: return true;
		case R.id.more: 
			i=new Intent(this,MoreGame.class);
			break;
		case R.id.stats: 
			i=new Intent(this,StatsView.class);
			break;
		default:break;		
		}
		if(i!=null) {
			startActivity(i);
			return true;
		}
		return false;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Intent i=null;
		switch (item.getItemId()) {
		case R.id.preference:
			i=new Intent(this,PrefManager.class);
			
			break;
		case R.id.help:
			Log.d("Cal24","Open Help Page");
			try {
				i=new Intent(this, HelpActivitiy.class);
			}
			catch(Throwable e) {
				Log.e("Cal24","Open Help Page Error",e);
			}
			
			break;
		default:break;		
		}
		if(i!=null) {
			try {
			startActivity(i);
			return true;
			}
			catch(Throwable e) {
				Log.e("Cal24","More Open Help Page Error",e);
			}
		}
		return false;
	}

	
	class StartAnimator implements Runnable {
		
		public void run() {
			try {
				if (view.startFrame()) {
					handler.postDelayed(new StartAnimator(), 100);
				}
				else {
					newPlay.setEnabled(true);
					nextSet.setEnabled(true);
					viewMore.setEnabled(true);
					Media.stop();
				}	
			} catch (Throwable e) {
				Log.e("Cal24", "Error in Timer", e);
			}
		}
		
		
	}

	class PickAnimator implements Runnable {
		private boolean animate;

		public PickAnimator(boolean animate) {
			super();
			this.animate = animate;
		}

		private void processed() {
			if (dialog != null) {
				dialog.dismiss();
				dialog = null;
			}
			newPlay.setEnabled(true);
			nextSet.setEnabled(true);
			viewMore.setEnabled(true);
			textContent.setVisibility(View.VISIBLE);
			if (solutions == null) {
				addScore(0);
			} else {
				Media.stop();
				clock.start();
				panelSwitcher.goLeft();
				Media.step(CalculateActivity.this);
			}
		}

		private void doContinue(boolean animate) {
			handler.postDelayed(new PickAnimator(animate), 100);
		}

		public void run() {
			try {
				if (animate) {
					if (view.nextFrame()) {
						doContinue(true);
					} else if (!isDone()) {
						dialog = new ProgressDialog(CalculateActivity.this);
						dialog.setMessage(getString(R.string.fetch_solution));
						dialog.show();
						doContinue(false);
					} else {
						processed();
					}
				} else if (isDone()) {
					processed();
				} else {
					doContinue(false);
				}
				
			} catch (Throwable e) {
				Log.e("Cal24", "Error in Timer", e);
			}
		}
	}

	class FetchTask extends AsyncTask<int[], Void, String[]> {

		@Override
		protected String[] doInBackground(int[]... params) {
			try {
				Calculation cal = new Calculation();
				List<String> res = cal.process(params[0]);
				return res.isEmpty() ? null : res.toArray(new String[res.size()]);
			} catch (Exception e) {
				Log.e("Cal24", "Error in fetch solutions", e);
			}
			return null;
		}

		@Override
		protected void onPostExecute(String[] result) {
			solutions = result;
			setDone();
		}
	}

}